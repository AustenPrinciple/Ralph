.PHONY: all opt fmt test clean

all:
	@ dune build lib --profile dev

opt:
	@ dune build lib --profile release

fmt:
	@ dune build @fmt --auto-promote

test:
	@ dune test

clean:
	@ dune clean
