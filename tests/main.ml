open DatEx
open Extensions
open Hashtbl

let print_proof fmt p =
  Format.fprintf fmt "%a" (List.fprint (fun x -> Format.fprintf fmt "%d" x)) p

let h0 = hash 0

let h00 = hash (2 * h0)

let h0000 = hash (2 * h00)

let h1234 = hash (hash (hash 1 + hash 2) + hash (hash 3 + hash 4))

let h5678 = hash (hash (hash 5 + hash 6) + hash (hash 7 + hash 8))

let root_hash () =
  let tree = MerkleInt.make 2 0 in
  let root = MerkleInt.root_hash tree in
  Alcotest.(check int) "Root of full 0 tree is h0000" h0000 root

let root_hash2 () =
  let tree = MerkleInt.init_list (List.init 4 succ) in
  let root = MerkleInt.root_hash tree in
  Alcotest.(check int) "Root of 1234 tree is h1234" h1234 root

let proof_tree () =
  let tree = MerkleInt.make 2 0 in
  let proof = MerkleInt.proof_tree 3 tree in
  let should_be = [ h0; h00 ] in
  Alcotest.(check (list int))
    "Proof tree of full 0 tree is full of zeroes" should_be proof

let proof_tree2 () =
  let l = List.init 9 succ in
  let tree = MerkleInt.init_list l in
  let proof = MerkleInt.proof_tree 0 tree in
  let hright = hash (hash (hash (hash 9 + h0) + h00) + h0000) in
  let should_be = [ hash 2; hash (hash 3 + hash 4); h5678; hright ] in
  Alcotest.(check (list int))
    "Non-empty tree gives complete proof" should_be proof

let pos_membership () =
  let l = List.init 5 succ in
  let tree = MerkleInt.init_list l in
  let root = MerkleInt.root_hash tree in
  let proofs = List.init 8 (fun x -> MerkleInt.proof_tree x tree) in
  List.iteri
    (fun i p ->
      let i' = if i < 5 then succ i else 0 in
      Alcotest.(check bool)
        (string_of_int i' ^ " is at its place in the tree")
        true (MerkleInt.mem i' p root))
    proofs

let root_set =
  [
    ("Zero-tree root", `Quick, root_hash); ("1234-tree root", `Quick, root_hash2);
  ]

let proof_set =
  [
    ("Zero-tree proof", `Quick, proof_tree);
    ("Depth 3 proof", `Quick, proof_tree2);
  ]

let membership_set = [ ("Positive membership", `Quick, pos_membership) ]

let () =
  Alcotest.run "Everything"
    [
      ("Root hashes", root_set);
      ("Proof trees", proof_set);
      ("Membership predicate", membership_set);
    ]
