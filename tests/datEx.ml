module DatInt = struct
  type t = int

  let default = 0

  let hash_one = Hashtbl.hash

  let hash_two x y = Hashtbl.hash (x + y)
end

module MerkleInt = Ralph.MerkleTree (DatInt)

module DatByte = struct
  type t = bytes

  let default = Bytes.empty

  let hash_one = Hashtbl.hash

  let hash_two x y = Hashtbl.hash (x + y)
end

module MerkleByte = Ralph.MerkleTree (DatByte)
