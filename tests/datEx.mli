module DatInt : Ralph.Data with type t = int

module MerkleInt : Ralph.T with type leaf_t = DatInt.t

module DatByte : Ralph.Data with type t = bytes

module MerkleByte : Ralph.T with type leaf_t = DatByte.t
