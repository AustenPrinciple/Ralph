# Merkle Trees in OCaml

This repository contains an implementation of Merkle Trees in the OCaml language,
and standard tooling and ci integration for future reference.

## Operations available on Merkle Trees :
- Creation from depth and default element
- Creation from list of elements
- In-place modification
- Insertion of new data block and subsequent doubling of size
- Extraction of root hash
- Extraction of a "proof tree" (a list of hashes in the correct order)
- Membership predicate given a proof tree and the root hash
